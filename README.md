# Recipe App API Proxy

NGiNX proxy app for our recipe app API

## Usage

### Environmental Variables

* `LISTEN_PORT` - Port to Listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (dfault: `9000`)
